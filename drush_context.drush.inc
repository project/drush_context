<?php // $Id$

/**
 * @file
 */

define('DRUSH_CONTEXT_DEFAULT_PATH', "export/context");

/**
 * Implementation of hook_help().
 */
function drush_context_help($section) {
  switch ($section) {
     case 'drush:context-list':
       return dt("Usage: drush [options] context-list\n\n"
                 ."List all available context.");
      case 'drush:context-export':
        return dt("Usage: drush [options] context-export <contex-name>\n\n"
                 ."Export the specified context to single .context files.\n"
                 ."<context> should be a comma-separated list. For example: context1,context2,context3.\n"
                 ."If <context> is omitted, all context are exported.\n\n"
                 ."Available options:\n"
                 ."--target-path=TARGET_PATH\n"
                 ."  Store exported context in the TARGET_PATH directory.\n"
                 ."  If omitted, the default path @default_path is used.\n",
                 array('@default_path' => DRUSH_CONTEXT_DEFAULT_PATH));
      case 'drush:context-import':
        return dt("Usage: drush [options] context-import <views>\n\n"
                 ."Import the specified context.\n"
                 ."Existing context will be deleted, or overridden if they are default context.\n"
                 ."<context> should be a comma-separated list of files, with an optional\n"
                 .".context extension. For example: file1,file2,file3.\n"
                 ."If <context> is a directory, all *.context files it contains will be imported.\n"
                 ."If omitted, the default path @default_path is used.\n",
                 array('@default_path' => DRUSH_CONTEXT_DEFAULT_PATH));
     case 'drush:context-delete':
       return dt("Usage: drush [options] context-delete <views>\n\n"
                 ."Delete the specified context.");
  }
}

/**
 * Implementation of hook_drush_command().
 */
function drush_context_drush_command() {
  $items = array();
  // ===========================================================================
  $items['context-export'] = array(
    'description' => 'Export one or more context.',
    'examples' => array(
      'drush c-out' => dt("Export all context available into site to folder !folder", array('!folder' => 'SITE_ROOT/' . DRUSH_CONTEXT_DEFAULT_PATH)),
      'drush c-out home_page' => dt("Export home_page context into folder !folder", array('!folder' => 'SITE_ROOT/' . DRUSH_CONTEXT_DEFAULT_PATH)),
      'drush c-out --folder=/var/backups/context' => dt("Export all context available into site to folder !folder", array('!folder' => '/var/backups/context')),
      'drush c-out home_page --folder=/var/backups/context' => dt("Export home_page context into folder !folder", array('!folder' => '/var/backups/context')),
      'drush c-out "home_page,users_pages"' => dt("Export home_page and users_pages context into folder !folder", array('!folder' => 'SITE_ROOT/' . DRUSH_CONTEXT_DEFAULT_PATH)),
    ),

    'callback' => 'drush_context_export',

    'aliases' => array('c-out'),
    'options' => array(
      'folder' => dt("Folder where save context files."),
    ),
    'arguments' => array(
      'contexts' => dt("Context name. Can export multiple context using comma separated list."),
    ),
  );

  // ===========================================================================
  $items['context-import'] = array(
    'description' => 'Import one or more context.',
    'examples' => array(
      'drush c-ls' => dt("List all context available into site."),
      'drush c-ls --namespace=content' => dt("List all context available into site for !namespace namespace.", array('!namespace' => 'content')),
    ),

    'callback' => 'drush_context_import',
    'arguments' => array(
      'contexts' => dt("Context names. Can import multiple context using comma separated list. Each context must have a file *.context into folder"),
    ),
    'options' => array(
      'enable' => dt("Enable or not loaded context."),
      'folder' => dt("Folder where load context files."),
    ),

    'aliases' => array('c-in'),
  );

  // ===========================================================================
  $items['context-list'] = array(
    'description' => 'List available context.',
    'examples' => array(
      'drush c-ls' => dt("List all context available into site."),
      'drush c-ls --namespace=content' => dt("List all context available into site for !namespace namespace.", array('!namespace' => 'content')),
    ),

    'callback' => 'drush_context_list',
    'options' => array(
      'namespace' => dt("Context namespace."),
    ),

    'aliases' => array('c-ls'),
  );

  // ===========================================================================
  $items['context-delete'] = array(
    'description' => 'Delete a context.',
    'examples' => array(
      'drush c-rm' => dt("Remove all context available into site."),
      'drush c-out home_page' => dt("Remove home_page context from site."),
      'drush c-out "home_page,users_pages"' => dt("Remove home_page and users_pages context from site."),
    ),

    'callback' => 'drush_context_delete',

    'aliases' => array('c-rm'),
    'arguments' => array(
      'contexts' => dt("Context name. Can export multiple context using comma separated list."),
    ),
  );
  return $items;
}

/**
 * Command callback: context export.
 */
function drush_context_export($contexts = NULL) {
  // Check for folder
  if (!$folder = drush_get_option('folder')) {
    $folder = DRUSH_CONTEXT_DEFAULT_PATH;
  }
  
  // Create folder or give error
  if (!file_check_directory($folder, FILE_CREATE_DIRECTORY)) {
    drush_log(dt('Unable to use !folder folder', array('!folder' => $folder)), 'error');
    return;
  } else {
    drush_log(dt('Save contexts into !folder folder', array('!folder' => $folder)));
  }

  // Get contexts
  $contexts = _drush_context_get_context($contexts);

  // Esport all context
  foreach ($contexts as $context) {
    $ctx  = context_load($context);
    $data = context_export($ctx);
    $file = realpath($folder) . '/' . $context . '.context';

    // Unable to use file_save_data, using PHP native function
    if (!$fp = fopen($file, 'wb')) {
      drush_log(dt('Unable to export context !context', array('!context' => $context)), 'error');
      return;
    } else {
      drush_log(dt('Context !context exported', array('!context' => $context)), 'success');
    }
    fwrite($fp, $data);
    fclose($fp);
  }

  return $result;
}

/**
 * Command callback: context import.
 */
function drush_context_import($contexts = NULL) {
  // Check for folder
  if (!$folder = drush_get_option('folder')) {
    $folder = DRUSH_CONTEXT_DEFAULT_PATH;
  }
  
  // List files to load
  if (is_null($contexts)) {
    $files = glob(realpath($folder) . '/*.context');
  } else {

    $contexts = str_replace(',', '.context,', $contexts . '.context');
    $contexts = str_replace(',', ',' . realpath($folder) . '/', realpath($folder) . '/' . $contexts);
    $files = explode(',', $contexts);
  }

  if (!is_array($files)) {
    $files = array($files);
  }

  // Load all files
  foreach ($files as $file) {
    if (!file_exists($file)) {
      $context = basename($file, ".context");
      drush_log(dt('Unable to import context !context', array('!context' => $context)), 'error');
    } else {
      $code = file_get_contents($file);
      eval($code);
      $success = FALSE;
      if (isset($context->name)) {
        $success = context_save($context);
      }

      if ($success) {
        drush_log(dt('Context !context loaded', array('!context' => $context->name)), 'success');
      } else {
        drush_log(dt('Unable to load context !context', array('!context' => $context->name)), 'error');
      }
    }
  }
  return;
}

/**
 * Command callback: context list.
 */
function drush_context_list() {
  // Get contexts
  $contexts = _drush_context_get_context();

  // List context
  drush_print(dt("Available context:") . "\n");
  foreach ($contexts as $context) {
    drush_print($context, 2);
  }

  // Total counter
  drush_print("\n" . format_plural(count($contexts), '1 context available.', '@count contexts available.'));
  return;
}

/**
 * Command callback: context delete.
 */
function drush_context_delete($contexts = NULL) {
  // Get contexts
  $contexts = _drush_context_get_context($contexts);

  // Confirm message
  $message = dt('Are you shure to delete "!contexts"', array('!contexts' => implode('", "', $contexts)));
  if (!drush_confirm($message)) {
    drush_log(dt("Context are not removed"));
    return;
  }

  // Delete contexts
  foreach ($contexts as $context) {
    $ctx = context_load($context);
    if (context_delete($ctx)) {
      drush_log(dt("Context !context_name removed", array('!context_name' => $context)), 'success');
    }
    else {
      drush_log(dt("Unable to remove context !context_name", array('!context_name' => $context)), 'error');
    }
  }
}

/**
 * Parse user input to extract context to manage.
 *
 * @param <string> $contexts
 *    String with context to parse. If NULL all available context are returned.
 *
 * @return <array>
 *    Array with all context to manage.
 */
function _drush_context_get_context($contexts = NULL) {
  $contexts_list = array();

  // Check for all contexts
  if (is_null($contexts)) {
    $contexts = context_context_list();
  }
  else {
    $contexts = explode(',', $contexts);
  }
  
  if (!is_array($contexts)) {
    $contexts = array($contexts);
  }

  // Normalize array
  foreach ($contexts as $context) {
    $contexts_list[] = $context;
  }

  return $contexts_list;
}