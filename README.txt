// $Id $

DESCRIPTION
-----------

Drush Context allows you to:

 * list
 * export
 * import
 * delete

context from the command line.

It provides the following commands:

 * context list
 * context import
 * context export
 * context delete

Run "drush help <command>" to see supported command line options and arguments.

REQUIREMENTS
------------
No special requirements on unix-like systems.
Drush Context uses the ordinary PHP commands for file and directory
handling, but it is still untested on Windows.

LICENSE
-------
Drush Context, Copyright (C) 2010 Marco Vito Moscaritolo
